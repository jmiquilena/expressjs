const express = require("express");
const http = require("http");

// setup server
const expressConfig = require("./config/express");
const app = express();
const server = http.createServer(app);

expressConfig(app);

const config = {
  port: 8080,
  ip: "127.0.0.1"
};

// start server

startServer = () => {
  app.shoppingCartBK = server.listen(config.port, config.ip, () => {
    console.log(
      `Express server listening on ${config.port}, in ${app.get("env")} mode`
    );
  });
};

setImmediate(startServer);

// Expose app

module.exports = app;
